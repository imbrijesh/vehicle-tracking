import { StorageService } from './../services/storage.service';
import { Router } from '@angular/router';
import { AuthService } from './../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { NotifierService } from 'angular-notifier';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginModel = {
    loginName: null,
    password: null
  };

  constructor(private authService: AuthService,
              private notifier: NotifierService,
              private router: Router,
              private storageService: StorageService) { }

  ngOnInit() {
  }

  login() {
    if (this.loginModel.loginName == null || this.loginModel.loginName.trim() === '') {
      this.notifier.notify('warning', 'Please enter username');
      return false;
    }
    if (this.loginModel.password == null || this.loginModel.password.trim() === '') {
      this.notifier.notify('warning', 'Please enter password');
      return false;
    }

    console.log(this.loginModel);
    this.authService.loginValidate(this.loginModel)
      .subscribe(data => {
        this.storageService.setIdentity(data.token);
        this.router.navigate(['/dashboard']);

      }, error => {
        if (error.error.status !== 400 && error.status !== 0) {
          this.notifier.notify('error', error.error.message);
        }
      });
  }

}

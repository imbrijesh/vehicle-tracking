import { VehicleResponse, GraphData } from './../viewmodel/vehicleResponse';
import { NotifierService } from 'angular-notifier';
import { VehicleService } from './../services/vehicle.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {


  responseList: Array<VehicleResponse> = [];

  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
  };
  public mbarChartLabels: number[] = [1, 2];
  public barChartType = 'bar';
  public barChartLegend = true;


  barChartData: Array<GraphData> = [];

  constructor(private notifier: NotifierService,
              private vehicleService: VehicleService) { }

  ngOnInit() {
    this.loadDashBoard();
  }

  loadDashBoard() {
    const vehicleModel = {
      gte: '2020-12-23T00:00:00.000Z',
      lt: '2021-1-5T00:00:00.000Z',
      useType: 'ADMIN',
      loginName: 'admin123'
    };
    this.vehicleService.vehicleTrack(vehicleModel)
      .subscribe(responseData => {
        this.responseList = [];
        let xy = [];
        this.barChartData = [];
        for (const data of responseData.docs) {
          const res = new VehicleResponse();
          res.pinno = data.pinno;
          res.totalDistance = data.totalDistance || 0;
          res.totalSeconds = data.totalSeconds || 0;
          this.responseList.push(res);

          const graphData = new GraphData();
          xy = [res.totalDistance, +data.totalSeconds];

          graphData.data.push(xy);
          graphData.label = data.pinno;
          this.barChartData.push(graphData);

        }
      }, error => {
          console.log(error);
      });
  }
}

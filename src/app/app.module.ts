import { DashboardComponent } from './dashboard/dashboard.component';
import { customNotifierOptions } from './customNotifierOptions';
import { TokenInterceptor } from './services/token.interceptor';
import { AuthService } from './services/auth.service';
import { RouterModule } from '@angular/router';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { SharedModule } from './shared/shared.module';
import { FormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NotifierModule } from 'angular-notifier';
import { ChartsModule } from 'ng2-charts';

@NgModule({
  declarations: [
    AppComponent,
      LoginComponent,
      DashboardComponent
   ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    RouterModule,
    SharedModule,
    ChartsModule,
    NotifierModule.withConfig(customNotifierOptions)
  ],
  providers: [AuthService,
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }

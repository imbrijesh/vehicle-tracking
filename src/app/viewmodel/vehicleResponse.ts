export class VehicleResponse {
    pinno: string;
    totalDistance: number;
    totalSeconds: number;
}

export class GraphData {
    data: any[] = new Array;
    label: number;
}

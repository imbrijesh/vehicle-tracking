import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  myStorage = localStorage;
  constructor() { }

  getIdentity(): any {
    const identity: string = this.myStorage.getItem('adminIdentity');
    return (identity) ? identity : null;
  }

  setIdentity(identity: any): any {
      this.myStorage.setItem('adminIdentity', identity);
  }
  logout() {
    this.myStorage.removeItem('adminIdentity');
  }


}

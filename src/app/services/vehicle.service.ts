import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class VehicleService {

  constructor(private http: HttpClient) {}

  vehicleTrack(vehicleModel: any) {
    return this.http.post<any>(`${environment.API_URL}/vehicletrack/main1`, vehicleModel);
  }

}

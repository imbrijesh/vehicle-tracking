import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) {}

  loginValidate(loginModel: any) {
    return this.http.post<any>(`${environment.API_URL}/users/authenticate`, loginModel);
  }

}

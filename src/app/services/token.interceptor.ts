import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

import { StorageService } from './storage.service';
import { NotifierService } from 'angular-notifier';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
    constructor(
        private storage: StorageService,
        private router: Router,
        private notifierService: NotifierService,
    ) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        console.log('inside interceptor');
        const excludePaths: string[] = [
            '/login'
        ];
        const matchedPath = excludePaths.filter(path => req.url.includes(path));
        const identity: any = this.storage.getIdentity();
        if (matchedPath.length === 0 || identity) {

            if (!identity) {
                this.router.navigate(['/login']);
            } else {
                req = req.clone({
                    setHeaders: {
                        Authorization: `Bearer ${identity}`
                    }
                });
            }
        }

        req = req.clone({
                        setHeaders: {
                            companyID: `AJAXFIORI`
                        }
                    });

        return next.handle(req).pipe(
            catchError( (err: any, caught: Observable<any>) => {
                return throwError(this.generalErrorHandler(err, caught));
            }
        ) );

    }

    generalErrorHandler(err: any, caught: Observable<any>): Observable<any> {
        if (err.status === 401) {
            // this.session.authenticate(null);
            this.storage.myStorage.removeItem('adminIdentity');
            alert('Your session expired. Please login to continue.');
            this.router.navigate(['/login']);
        } else if (err.status === 400) {
            this.notifierService.notify('error', err.error.message);
        } else if (err.status === 0) {
          this.notifierService.notify('error', 'A network error occurred, check your internet connection and try again');
        }
        return err;
    }


}
